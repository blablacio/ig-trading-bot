FROM python:3.7-slim

RUN apt update && apt install -y git gcc

WORKDIR /app

ADD requirements/base.txt requirements/

RUN pip install -r requirements/base.txt

ADD . /app
