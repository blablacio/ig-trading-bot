from django.contrib import admin

from apps.trading.models import Exchange, ExchangeInstrument


class ExchangeAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ExchangeInstrumentAdmin(admin.ModelAdmin):
    list_display = ('name', 'exchange', 'identifier', 'category', 'currency')
    search_fields = ('name', 'identifier')


admin.site.register(Exchange, ExchangeAdmin)
admin.site.register(ExchangeInstrument, ExchangeInstrumentAdmin)
