import numpy as np


def is_gartley(moves, err_allowed):
    xa = moves[0]
    ab = moves[1]
    bc = moves[2]
    cd = moves[3]

    ab_range = np.array([0.618 - err_allowed, 0.618 + err_allowed]) * abs(xa)
    bc_range = np.array([0.382 - err_allowed, 0.886 + err_allowed]) * abs(ab)
    cd_range = np.array([1.27 - err_allowed, 1.618 + err_allowed]) * abs(bc)

    if xa > 0 > ab and bc > 0 > cd:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return 1
        else:
            return np.nan

    elif xa < 0 < ab > bc and cd > 0:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return -1
        else:
            return np.nan
    else:
        return np.nan


def is_butterfly(moves, err_allowed):
    xa = moves[0]
    ab = moves[1]
    bc = moves[2]
    cd = moves[3]

    ab_range = np.array([0.786 - err_allowed, 0.786 + err_allowed]) * abs(xa)
    bc_range = np.array([0.382 - err_allowed, 0.886 + err_allowed]) * abs(ab)
    cd_range = np.array([1.618 - err_allowed, 2.618 + err_allowed]) * abs(bc)

    if xa > 0 > cd and ab < 0 < bc:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return 1
        else:
            return np.nan

    elif xa < 0 < ab > bc and cd > 0:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return -1
        else:
            return np.nan
    else:
        return np.nan


def is_bat(moves, err_allowed):
    xa = moves[0]
    ab = moves[1]
    bc = moves[2]
    cd = moves[3]

    ab_range = np.array([0.382 - err_allowed, 0.5 + err_allowed]) * abs(xa)
    bc_range = np.array([0.382 - err_allowed, 0.886 + err_allowed]) * abs(ab)
    cd_range = np.array([1.618 - err_allowed, 2.618 + err_allowed]) * abs(bc)

    if xa > 0 > ab < bc and cd < 0:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return 1
        else:
            return np.nan

    elif xa < 0 < ab and bc < 0 < cd:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return -1
        else:
            return np.nan
    else:
        return np.nan


def is_crab(moves, err_allowed):
    xa = moves[0]
    ab = moves[1]
    bc = moves[2]
    cd = moves[3]

    ab_range = np.array([0.382 - err_allowed, 0.618 + err_allowed]) * abs(xa)
    bc_range = np.array([0.382 - err_allowed, 0.886 + err_allowed]) * abs(ab)
    cd_range = np.array([2.24 - err_allowed, 3.618 + err_allowed]) * abs(bc)

    if xa > 0 > ab < bc and cd < 0:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return 1
        else:
            return np.nan

    elif xa < 0 < ab > bc and cd > 0:
        if (
                ab_range[0] < abs(ab) < ab_range[1] and
                bc_range[0] < abs(bc) < bc_range[1] and
                cd_range[0] < abs(cd) < cd_range[1]
        ):
            return -1
        else:
            return np.nan
    else:
        return np.nan
