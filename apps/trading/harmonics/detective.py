import logging
import time
from io import BytesIO

import numpy
import pandas
from django.conf import settings
from django.core.files.images import ImageFile
from django_redis import get_redis_connection
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

from apps.trading.harmonics.patterns import is_gartley, is_butterfly, is_bat, is_crab
from apps.trading.harmonics.utils import peak_detect
from apps.trading.helpers.cache import disconnect_stream, revoke_order, purge_order_cache
from apps.trading.tasks import submit_order
from apps.user.models import Order

logger = logging.getLogger(__name__)
redis_connection = 'orders'

if settings.TEST:
    redis_connection = 'test-orders'

cache = get_redis_connection(redis_connection)


class PatternDetective:
    """
    Keeps track of incoming Lightstreamer data, detects patterns
    as they occur and triggers order submission.
    """

    minimum_frames = 100
    maximum_frames = 2000
    entry_distance = 200
    error_tolerance = 5 / 100

    def __init__(self, *args, **kwargs):
        self.data_frame = pandas.DataFrame(columns=['price', 'moving_average'])
        self.pattern = None
        self.pattern_label = None
        self.order_data = kwargs.pop('order_data')
        self.moving_average = self.order_data.pop('moving_average')

    def __call__(self, *args, **kwargs):
        logger.info(
            'Running pattern detective for order {}, take {}'.format(
                self.order_data.get('id'),
                len(self.data_frame) + 1
            )
        )
        frame = args[0]

        if not frame.get('values', {}).get('BID_CLOSE'):
            return

        # Get order
        try:
            order = Order.objects.get(pk=self.order_data.get('id'))
        except Order.DoesNotExist:
            logger.warning(
                'Order %s was deleted, bailing out...',
                self.order_data.get('id')
            )
            # Revoke order and clear cache
            revoke_order(self.order_data.get('id'))
            purge_order_cache(self.order_data.get('id'), delete_data=True)
            # Break execution
            return

        index = pandas.to_datetime(frame['values']['UTM'], unit='ms')
        price = numpy.float64(frame['values']['BID_CLOSE'])
        self.data_frame.at[index, 'price'] = price

        # Save price data in cache
        cache.execute_command(
            'ts.add',
            'order:{}:chart:price'.format(order.pk),
            frame['values']['UTM'],
            price
        )

        # Generate moving average data
        self.data_frame.moving_average = self.data_frame.price.rolling(
            window=self.moving_average,
            center=False
        ).mean()

        # Save MA data in cache
        if not numpy.isnan(self.data_frame.moving_average[-1]):
            cache.execute_command(
                'ts.add',
                'order:{}:chart:ma'.format(order.pk),
                frame['values']['UTM'],
                self.data_frame.moving_average[-1]
            )

        # Set order last update time in cache
        cache.hset('order:{}'.format(order.pk), 'last_update', int(time.time()))

        if not self.pattern:
            # Trim data
            # max_length = self.maximum_frames + self.moving_average + self.entry_distance
            # self.data_frame = self.data_frame[
            #     ~self.data_frame.index.duplicated(keep='last')
            # ].tail(max_length)

            # Return if not enough data
            if len(self.data_frame) < self.minimum_frames + self.moving_average:
                return

            peaks = peak_detect(self.data_frame)

            if not peaks:
                return

            XA = peaks['prices'][1] - peaks['prices'][0]
            AB = peaks['prices'][2] - peaks['prices'][1]
            BC = peaks['prices'][3] - peaks['prices'][2]
            CD = peaks['prices'][4] - peaks['prices'][3]

            moves = [XA, AB, BC, CD]

            gartley = is_gartley(moves, self.error_tolerance)
            butterfly = is_butterfly(moves, self.error_tolerance)
            bat = is_bat(moves, self.error_tolerance)
            crab = is_crab(moves, self.error_tolerance)

            harmonics = numpy.array([gartley, butterfly, bat, crab])
            labels = ['Gartley', 'Butterfly', 'Bat', 'Crab']

            if numpy.any(harmonics == 1) or numpy.any(harmonics == -1):
                for j in range(0, len(harmonics)):
                    if harmonics[j] == 1 or harmonics[j] == -1:
                        sentiment = 'Bearish' if harmonics[j] == -1 else 'Bullish'
                        self.pattern_label = '{} {}'.format(sentiment, labels[j])
                        self.pattern = peaks

                        # Cast to UNIX timestamps
                        indexes = (peaks['indexes'].astype(numpy.int64) // 10 ** 6).tolist()
                        prices = peaks['prices']

                        for idx, price in dict(zip(indexes, prices)).items():
                            cache.execute_command(
                                'ts.add',
                                'order:{}:chart:pattern'.format(order.pk),
                                idx,
                                price
                            )

                        logger.info(
                            'Found a {} pattern for order {}!'.format(
                                sentiment,
                                self.order_data.get('id')
                            )
                        )

                        # Update order
                        order.direction = harmonics[j]
                        order.status = Order.STATUS_PATTERN_FOUND
                        order.save()
        else:
            crossover = self.get_crossover()

            if crossover is not None:
                # Define entry price as last MA crossover
                entry_price = crossover.price[-1]
                # Calculate stoploss price as a 61.8% Fibonacci retracement of XA
                stoploss_multiplier = 1.618
                exit_multiplier = 0.618
                x_point = self.pattern['prices'][0]
                a_point = self.pattern['prices'][1]
                c_point = self.pattern['prices'][3]
                d_point = self.pattern['prices'][4]
                xa_diff = abs(x_point - a_point)
                cd_diff = abs(c_point - d_point)

                # In case of bullish pattern (long trade):
                #   - subtract XA difference multiplied by 1.618 from X point
                #   - add D point to CD difference multiplied by 0.618
                if order.direction == Order.DIRECTION_BUY:
                    stoploss_price = x_point - (xa_diff * stoploss_multiplier)
                    exit_price = d_point + (cd_diff * exit_multiplier)

                    # Reset current pattern and start a new cycle
                    if stoploss_price > entry_price or exit_price < entry_price:
                        logger.info('Stoploss > entry or exit < entry, restarting...')
                        self.reset()
                        # Break execution
                        return

                # In case of bearish pattern (short trade):
                #   - add X point to XA difference multiplied by 1.618
                #   - subtract CD difference multiplied by 0.618 from D point
                elif order.direction == Order.DIRECTION_SELL:
                    stoploss_price = x_point + (xa_diff * stoploss_multiplier)
                    exit_price = d_point - (cd_diff * exit_multiplier)

                    # Reset current pattern and start a new cycle
                    if stoploss_price < entry_price or exit_price > entry_price:
                        logger.info('Stoploss < entry or exit > entry, restarting...')
                        self.reset()
                        # Break execution
                        return

                logger.info(
                    'Crossover found for order %s at %s',
                    self.order_data.get('id'),
                    crossover.price[-1]
                )
                # Save last crossover point in cache
                crossover_ts = (crossover.index.astype(numpy.int64) // 10 ** 6).astype(str)

                cache.execute_command(
                    'ts.add',
                    'order:{}:chart:crossover'.format(order.pk),
                    crossover_ts[-1],
                    crossover.moving_average[-1]
                )

                # Plot chart with Matplotlib as a backup
                figure = self.plot()
                # Update order
                order.chart.save(order.chart.name, ImageFile(figure))
                order.stoploss_price = stoploss_price
                order.exit_price = exit_price
                order.entry_price = entry_price
                order.status = Order.STATUS_PATTERN_CONFIRMED
                order.save()

                logger.info('Trivia: %s', self.data_frame.to_json())
                submit_order(self.order_data)

                disconnect_stream(self.order_data.get('id'))

    def get_crossover(self):
        end = self.pattern['end']
        # Get indexes of crossovers between price and moving average
        crossover_idx = numpy.argwhere(
            numpy.diff(
                numpy.sign(self.data_frame.price[end:] - self.data_frame.moving_average[end:])
            )
        ).flatten().tolist()

        # Bail out if there's no crossover
        if not crossover_idx:
            return

        logger.info(
            'Crossover found for order %s: %s',
            self.order_data.get('id'),
            crossover_idx[-1]
        )

        return self.data_frame.loc[self.pattern['end']:].iloc[crossover_idx]

    def plot(self):
        # Generate chart image placeholder
        src = BytesIO()
        # Create figure and canvas
        fig = Figure()
        canvas = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        # Set title
        ax.set_title(self.pattern_label)

        # Check if this is a new order without a plot
        start = self.pattern['start']

        # Plot price
        ax.plot(
            self.data_frame.loc[start:],
        )
        # Plot pattern
        ax.plot(
            self.pattern['indexes'],
            self.pattern['prices'],
            c='r'
        )

        # Get crossovers between price and MA
        crossover = self.get_crossover()

        if crossover is not None:
            logger.info(
                'Plotting crossover for order %s at %s, pattern: %s',
                self.order_data.get('id'),
                crossover.index[-1],
                self.pattern
            )
            ax.plot(
                crossover.index,
                crossover.moving_average,
                'ko'
            )

        canvas.print_png(src)

        return src

    def reset(self):
        # Reset current state
        self.pattern = None
        self.pattern_label = None
        self.data_frame = pandas.DataFrame(columns=['price', 'moving_average'])
        # Purge order data from cache
        purge_order_cache(self.order_data.get('id'), delete_order=False, delete_data=True)
