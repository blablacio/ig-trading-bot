import numpy as np
from scipy.signal import argrelextrema


def peak_detect(data_frame, order=10):
    price = data_frame.price.values
    # Populate indexes of maximum and minimum values
    max_idx = list(argrelextrema(price, np.greater, order=order)[0])
    min_idx = list(argrelextrema(price, np.less, order=order)[0])
    # Combined maximum and minimum indexes in one
    idx = max_idx + min_idx + [len(price) - 1]

    # Bail out if there are less than 5 data points
    if len(idx) < 5:
        return

    # Sort indexes of minimum and maximum data points
    idx.sort()

    # Get a slice of the last 5 peaks/throughs
    indexes = data_frame.index[idx[-5:]]

    # Populate start and end indexes of pattern
    start = min(indexes)
    end = max(indexes)

    # Populate current pattern with price data corresponding to peak/through indexes
    prices = price[idx[-5:]].tolist()

    return {
        'indexes': indexes,
        'prices': prices,
        'start': start,
        'end': end
    }
