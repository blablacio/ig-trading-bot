from django.db import models


class Exchange(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class ExchangeInstrument(models.Model):
    exchange = models.ForeignKey('trading.Exchange', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    identifier = models.CharField(max_length=256)
    currency = models.CharField(max_length=32, blank=True)
    category = models.CharField(max_length=256)

    def __str__(self):
        return self.name
