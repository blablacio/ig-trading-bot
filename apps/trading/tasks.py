import logging
import time

import requests
from django.conf import settings
from django.utils import timezone
from django_redis import get_redis_connection
from huey import crontab
from huey.contrib.djhuey import db_task, periodic_task
from trading_ig import IGService, IGStreamService
from trading_ig.lightstreamer import Subscription

from apps.trading.models import ExchangeInstrument
from apps.user.models import Order

logger = logging.getLogger(__name__)
cache = get_redis_connection('orders')
ls_clients = {}


@db_task()
def pattern_scan(order_data):
    from apps.trading.harmonics.detective import PatternDetective

    pattern_detective = PatternDetective(order_data=order_data)

    service = IGService(
        order_data.get('username'),
        order_data.get('password'),
        order_data.get('api_key'),
        order_data.get('type')
    )
    stream_service = IGStreamService(service)
    session = stream_service.create_session()
    accounts = session['accounts']

    stream_service.connect(accounts[0].get('accountId'))

    subscription_prices = Subscription(
        mode='MERGE',
        items=[
            'CHART:{}:{}'.format(
                order_data.get('instrument'),
                settings.IG_SETTINGS['TIMEFRAME_MAPPING'][order_data.get('timeframe')]
            )
        ],
        fields=['UTM', 'BID_CLOSE'],
    )

    subscription_prices.addlistener(pattern_detective)
    subscription = stream_service.ls_client.subscribe(subscription_prices)

    ls_clients[str(order_data.get('id'))] = {
        'client': stream_service.ls_client,
        'subscription': subscription
    }

    stream_service.ls_client._join()


@db_task()
def submit_order(order_data):
    order_id = order_data.get('id')
    order = Order.objects.get(pk=order_id)
    account = order.account

    service = IGService(
        account.username,
        account.password,
        account.api_key,
        account.type
    )

    # Define direction map
    direction_map = {
        1: 'BUY',
        -1: 'SELL'
    }

    service.create_session()
    position = service.create_open_position(
        currency_code=order.currency,
        direction=direction_map[order.direction],
        epic=order.instrument.identifier,
        expiry='-',
        force_open=True,
        guaranteed_stop=False,
        level=str(order.entry_price),
        limit_distance=None,
        limit_level=str(order.entry_price),
        order_type='LIMIT',
        quote_id=None,
        size=str(order.size),
        stop_distance=None,
        stop_level=str(order.stoploss_price)
    )
    logger.info('Position information for order %s: %s', order_id, position)

    # Set deal reference and ID
    order.deal_reference = position.get('dealReference')
    order.deal_id = position.get('dealId')

    if position.get('dealStatus') == 'ACCEPTED':
        from apps.trading.helpers.orders import OrderMonitor

        order.status = Order.STATUS_ENTRY_SUBMITTED
        order.entry_time = timezone.now()
        monitor_task = monitor_order(
            order_data={
                'username': order_data.get('username'),
                'password': order_data.get('password'),
                'api_key': order_data.get('api_key'),
                'type': order_data.get('type'),
                'currency': order.currency,
                'deal_reference': position.get('dealReference')
            },
            monitor_type=OrderMonitor.ENTRY
        )
        # Replace task ID in cache with monitor order task ID
        cache.hset('order:{}'.format(order.pk), 'task_id', monitor_task.task.id)
    else:
        from apps.trading.helpers.cache import purge_order_cache

        rejection_reason = position.get('reason')
        # Get historical activity in order to find out what the error is
        # Look only five minutes back -- i.e. 5 minutes * 60 seconds * 1000 milliseconds
        history = service.fetch_account_activity_by_period(milliseconds=5 * 60 * 1000)
        # Find the relevant item by deal ID
        item = next(history[history.dealId == position.get('dealId')].itertuples(), None)
        # Log account activity history
        logger.warning('Account activity history for order %s: %s', order_id, history.to_string())
        # And set order status
        order.status = Order.STATUS_ERROR
        # Set order error based on reason returned in create position call
        # and result column from account activity history call
        order.error = 'Reason: {}, result: {}'.format(
            rejection_reason,
            getattr(item, 'result', None)
        )
        # Purge order cache
        purge_order_cache(order.pk)

    order.save()


@db_task()
def monitor_order(order_data, monitor_type):
    from apps.trading.helpers.orders import OrderMonitor

    order_monitor = OrderMonitor(order_data=order_data, monitor_type=monitor_type)

    service = IGService(
        order_data.get('username'),
        order_data.get('password'),
        order_data.get('api_key'),
        order_data.get('type')
    )
    stream_service = IGStreamService(service)
    session = stream_service.create_session()
    accounts = session['accounts']
    account_id = accounts[0].get('accountId')

    stream_service.connect(account_id)

    subscription_trades = Subscription(
        mode='DISTINCT',
        items=[
            'TRADE:{}'.format(account_id)
        ],
        fields=['CONFIRMS'],
    )

    subscription_trades.addlistener(order_monitor)
    subscription = stream_service.ls_client.subscribe(subscription_trades)

    ls_clients[str(order_data.get('id'))] = {
        'client': stream_service.ls_client,
        'subscription': subscription
    }

    stream_service.ls_client._join()


@periodic_task(crontab(minute='*'))
def check_orders():
    from apps.trading.helpers.cache import get_order_cache, restart_order, purge_order_cache

    # Get all active orders
    orders = cache.smembers('active_orders')
    # Get order data from cache
    data = get_order_cache(orders)
    # Define multiplier map, so that missing an update for lower frequency updates
    # like 1/3/5/15/30/45/60m or 1/2/3/4/6/12h will trigger an order restart, but
    # higher frequency like 1s won't
    multiplier = settings.TIMEFRAME_TIMEOUT_MULTIPLIER
    # Calculate overdue orders
    try:
        overdue_orders = {
            o: d for o, d in data.items()
            if d['last_update'] < time.time() - (d['timeframe'] * multiplier[d['timeframe']])
        }
    except Exception as e:
        logger.error('Unexpected error while checking for overdue orders: %s', e)
        logger.info('Order data: %s', data)
    else:
        for order, data in overdue_orders.items():
            restart_order(order, data.get('stage'), revoke=True)
            purge_order_cache(order, delete_order=False, delete_data=True)


@db_task()
def sync_instruments(exchange, currency=None):
    url = 'https://demo-api.ig.com/gateway/deal'
    s = requests.Session()
    s.headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json; charset=UTF-8',
        'VERSION': '2',
        'X-IG-API-KEY': '72141f3228e760638dd9bb07b43ef1096bc8035b'
    }
    data = {
        'identifier': 'rogiervandenberg-Demo',
        'password': '63bdd51d49'
    }
    r = s.post('{}/session'.format(url), json=data)

    s.headers.update({
        'X-SECURITY-TOKEN': r.headers['X-SECURITY-TOKEN'],
        'CST': r.headers['CST'],
        'VERSION': '1'
    })

    def explore(market, node=None):
        if not node:
            node = market

        r = s.get('{}/marketnavigation/{}'.format(url, node['id']))

        if 'errorCode' in r.json():
            logger.warning('Unexpected error while syncing instruments: %s', r.json())
            # Wait for a minute
            time.sleep(60)
            # And retry
            explore(market, node)

        if isinstance(r.json()['nodes'], list):
            for node in r.json()['nodes']:
                # Pause for 1 second to make sure API call quota is not exceeded
                time.sleep(1)
                # Get nodes recursively
                explore(market, node)

        if isinstance(r.json()['markets'], list):
            for node in r.json()['markets']:
                instrument = s.get('{}/markets/{}'.format(url, node['epic']))
                # Pause for 1 second to make sure API call quota is not exceeded
                time.sleep(1)

                try:
                    currency_code = instrument.json()['instrument']['currencies'][0]['name']
                except KeyError:
                    logger.warning(
                        'Unexpected error while getting instrument currency: %s',
                        instrument.json()
                    )
                    continue

                if currency and currency_code != currency:
                    continue

                ExchangeInstrument.objects.update_or_create(
                    identifier=node['epic'],
                    exchange_id=exchange,
                    defaults={
                        'name': node['instrumentName'],
                        'category': market['name'],
                        'currency': currency_code
                    }
                )

    markets = s.get('{}/marketnavigation'.format(url))

    for m in markets.json().get('nodes'):
        explore(m)

    s.delete('{}/session'.format(url))
