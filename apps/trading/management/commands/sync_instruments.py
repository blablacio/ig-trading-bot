from django.core.management import BaseCommand

from apps.trading.models import Exchange
from apps.trading.tasks import sync_instruments


class Command(BaseCommand):
    help = 'Syncs all tradable instruments'

    def add_arguments(self, parser):
        parser.add_argument('--exchange', default='IG', nargs='?', type=str)
        parser.add_argument('--currency', default='EUR', nargs='?', type=str)

    def handle(self, *args, **options):
        try:
            exchange_obj = Exchange.objects.get(name=options.get('exchange'))
        except Exchange.DoesNotExist:
            raise ValueError('Exchange %s does not exist', options.get('exchange'))

        sync_instruments(exchange=exchange_obj.pk, currency=options.get('currency'))
