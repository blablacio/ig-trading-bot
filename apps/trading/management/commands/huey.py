import logging
import os

from django.core import management
from django.core.management import BaseCommand
from django_redis import get_redis_connection

from apps.trading.helpers.cache import restart_order

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Wraps run_huey command,
    """

    @staticmethod
    def restart_stale_orders():
        # Get order cache
        cache = get_redis_connection('orders')
        # Fetch active orders
        orders = cache.smembers('active_orders')

        for order in orders:
            order = order
            # Get order data
            data = cache.hgetall('order:{}'.format(order))
            # Log order restart event
            logger.info('Restarting order %s at stage %s...', order, data.get('stage'))
            # Restart order at given stage
            # TODO:
            #  Make the process smarter by checking for orders with
            #  at least 1 missing period and calling purge_order_cache.
            restart_order(order, data.get('stage'))

    def add_arguments(self, parser):
        parser.add_argument(
            '--no-periodic',
            '-n',
            action='store_false'
        )
        parser.add_argument(
            '--worker-type',
            '-k',
            default=os.getenv('HUEY_WORKER_TYPE', 'greenlet'),
            nargs='?',
            type=str
        )
        parser.add_argument(
            '--workers',
            '-w',
            default=os.getenv('HUEY_WORKER_COUNT', 100),
            nargs='?',
            type=int
        )
        parser.add_argument(
            '--flush-locks',
            '-f',
            action='store_true'
        )
        parser.add_argument(
            '--restart-stale-orders',
            '-r',
            action='store_true'
        )

    def handle(self, *args, **options):
        # Restart all stale orders
        if options.get('restart_stale_orders'):
            self.restart_stale_orders()

        # Forward to real run_huey command
        management.call_command(
            'run_huey',
            flush_locks=options.get('flush_locks'),
            no_periodic=options.get('no_periodic'),
            worker_type=options.get('worker-type'),
            workers=options.get('workers')
        )
