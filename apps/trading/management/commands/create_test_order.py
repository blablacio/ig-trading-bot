from django.conf import settings
from django.core.management import BaseCommand

from apps.trading.models import ExchangeInstrument
from apps.user.models import ExchangeAccount, Order


class Command(BaseCommand):
    help = 'Simulates an order for testing purposes'

    def add_arguments(self, parser):
        parser.add_argument('--currency', '-c', default='EUR', nargs='?', type=str)

    def handle(self, *args, **options):
        try:
            account = ExchangeAccount.objects.first()
        except ExchangeAccount.DoesNotExist:
            print('No active accounts found!')

        try:
            instrument = ExchangeInstrument.objects.first()
        except ExchangeInstrument.DoesNotExist:
            print('No instruments found!')

        Order.objects.create(
            user=account.user,
            instrument=instrument,
            currency=options.get('currency'),
            account=account,
            size=100,
            timeframe=settings.TIMEFRAME_1S
        )
