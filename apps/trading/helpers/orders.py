import logging

from django.utils import timezone
from django_redis import get_redis_connection
from trading_ig import IGService

from apps.trading.helpers.cache import purge_order_cache, disconnect_stream
from apps.trading.tasks import monitor_order
from apps.user.models import Order

logger = logging.getLogger(__name__)
cache = get_redis_connection('orders')


class OrderMonitor:
    """
    Keeps track of incoming Lightstreamer data, detects trades
    as they occur and triggers exit order submission.
    """

    ENTRY = 'ENTRY'
    EXIT = 'EXIT'

    def __init__(self, *args, **kwargs):
        self.frame_count = 0
        self.monitor_type = kwargs.pop('monitor_type')
        self.order_data = kwargs.pop('order_data')

    def __call__(self, *args, **kwargs):
        logger.info(
            'Running {} order monitor, take {}'.format(self.monitor_type, self.frame_count + 1)
        )
        trades = args[0]
        logger.warning('Trades received: %s', trades)

        # Check if we have a notification for deal reference
        # we're interested into and submit a new order if so
        deal_reference = self.order_data.get('deal_reference')
        closed = 'FULLY_CLOSED'

        filtered_trades = trades[
            (trades.dealReference == deal_reference) & (trades.status == closed)
        ]
        trade = next(filtered_trades.itertuples(), None)

        if trade:
            order = Order.objects.get(pk=self.order_data.get('id'))

            if self.monitor_type == self.ENTRY:
                self.handle_entry_trade(order)
            elif self.monitor_type == self.EXIT:
                self.handle_exit_trade(order)

            # Break execution and cleanup
            disconnect_stream(self.order_data.get('id'))

    def handle_entry_trade(self, order):
        """Monitoring entry trade for execution so exit trade can be submitted"""

        # Define direction map, which is opposite of the entry order direction map
        # On entry we need to BUY on positive sentiment, on exit we need to SELL instead
        direction_map = {
            1: 'SELL',
            -1: 'BUY'
        }

        # Initialize IG API connection
        service = IGService(
            self.order_data.get('username'),
            self.order_data.get('password'),
            self.order_data.get('api_key'),
            self.order_data.get('type')
        )
        service.create_session()
        # Create new order
        position = service.create_open_position(
            currency_code=self.order_data.get('currency'),
            direction=direction_map[self.order_data.get('direction')],
            epic=self.order_data.get('instrument'),
            expiry='-',
            force_open=True,
            guaranteed_stop=False,
            level=str(self.order_data.get('exit_price')),
            limit_distance=None,
            limit_level=str(self.order_data.get('exit_price')),
            order_type='LIMIT',
            quote_id=None,
            size=str(self.order_data.get('size')),
            stop_distance=None
        )
        # Log position information
        logger.info('Position information for order %s: %s', self.order_data.get('id'), position)

        # Set deal reference and ID
        order.deal_reference = position.get('dealReference')
        order.deal_id = position.get('dealId')

        if position.get('dealStatus') == 'ACCEPTED':
            order.status = Order.STATUS_EXIT_SUBMITTED
            order.exit_deal_id = position.get('dealId')
            order.exit_deal_reference = position.get('dealReference')
            order.entry_time = timezone.now()
            # Save order
            order.save()
        else:
            rejection_reason = position.get('reason')
            # Get historical activity in order to find out what the error is
            # Look only five minutes back -- i.e. 5 minutes * 60 seconds * 1000 milliseconds
            history = service.fetch_account_activity_by_period(milliseconds=5 * 60 * 1000)
            # Find the relevant item by deal ID
            item = next(history[history.dealId == position.get('dealId')].itertuples(), None)
            # Log account activity history
            logger.warning(
                'Account activity history for order %s: %s',
                self.order_data.get('id'),
                history.to_string()
            )
            # And set order status
            order.status = Order.STATUS_ERROR
            # Set order error based on reason returned in create position call
            # and result column from account activity history call
            order.error = 'Reason: {}, result: {}'.format(
                rejection_reason,
                getattr(item, 'result', None)
            )
            # Purge order cache
            purge_order_cache(order.pk)
            # Save order
            order.save()
            # Cleanup
            disconnect_stream(order.pk)

        # Start monitoring for exit trade
        order_data = self.order_data.update({
            'deal_reference': position.get('dealReference')
        })
        monitor_task = monitor_order(
            order_data=order_data,
            monitor_type=self.EXIT
        )
        # Replace task ID in cache with monitor order task ID
        # Replace stage in cache with monitor stage
        cache.hmset(
            'order:{}'.format(order.pk),
            {
                'task_id': monitor_task.task_id,
                'stage': 'monitor'
            }
        )

    def handle_exit_trade(self, order):
        """Monitoring exit trade for execution so order can be marked as completed"""

        order.status = Order.STATUS_PROCESSED
        order.save()
        # Purge order cache
        purge_order_cache(order.pk)
