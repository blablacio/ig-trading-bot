import logging
import time

from django_redis import get_redis_connection
from huey.contrib.djhuey import revoke_by_id

from apps.trading.tasks import pattern_scan, monitor_order, ls_clients
from apps.user.models import Order

cache = get_redis_connection('orders')
logger = logging.getLogger(__name__)


def get_order_cache(orders):
    # Fetch order data from cache
    order_data = {
        order: cache.hgetall('order:{}'.format(order)) for order in orders
    }
    # Convert data to appropriate types
    order_data = {
        order: {
            k: int(v) if v.isdigit() else v for k, v in order_data[order].items()
        } for order in order_data
    }

    return order_data


def purge_order_cache(order_id, delete_order=True, delete_data=False):
    logger.info('Purging order %s cache...', order_id)

    if delete_order:
        cache.srem('active_orders', order_id)
        cache.delete('order:{}'.format(order_id))

    if delete_data:
        cache.delete(
            *[
                'order:{}:chart:price'.format(order_id),
                'order:{}:chart:ma'.format(order_id),
                'order:{}:chart:pattern'.format(order_id)
            ]
        )


def revoke_order(order):
    logger.info('Revoking order %s...', order)
    order_data = cache.hgetall('order:{}'.format(order))
    task_id = order_data.get('task_id')
    revoke_by_id(task_id)
    disconnect_stream(order)


def disconnect_stream(order):
    order = str(order)
    # Disconnect lightstreamer
    if order in ls_clients:
        logger.info('Unsubscribing and disconnecting for order %s.', order)
        ls_clients[order]['client'].unsubscribe(ls_clients[order]['subscription'])
        ls_clients[order]['client'].disconnect()


def restart_order(order, stage, revoke=False):
    if revoke:
        revoke_order(order)

    logger.info('Restarting order %s...', order)
    logger.info(cache.hgetall('order:{}'.format(order)))
    # Define step map
    stage_map = {
        'scan': pattern_scan,
        'monitor': monitor_order
    }
    # Find task
    try:
        instance = Order.objects.get(pk=order)
    except Order.DoesNotExist:
        # Remove order cache, revoke order and return
        purge_order_cache(order, delete_data=True)
        revoke_order(order)

        return

    # Get order data needed for pattern scan task
    order_data = {
        'id': instance.pk,
        'exchange': instance.account.exchange.name,
        'type': instance.account.type,
        'username': instance.account.username,
        'password': instance.account.password,
        'api_key': instance.account.api_key,
        'api_secret': instance.account.api_secret,
        'instrument': instance.instrument.identifier,
        'timeframe': instance.timeframe,
        'moving_average': instance.moving_average
    }
    result = stage_map[stage](order_data)
    # Set order data cache
    cache.hmset(
        'order:{}'.format(instance.pk),
        {
            'task_id': result.task.id,
            'reset': 1,
            'last_reset': round(time.time() * 1000)
        }
    )
