from django.contrib import admin

from apps.user.models import ExchangeAccount, Order


class ExchangeAccountAdmin(admin.ModelAdmin):
    model = ExchangeAccount
    list_display = ('exchange', 'alias', 'type', 'created', 'updated')


class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_display = (
        'instrument', 'size', 'timeframe', 'entry_price', 'created', 'updated',
    )


admin.site.register(ExchangeAccount, ExchangeAccountAdmin)
admin.site.register(Order, OrderAdmin)
