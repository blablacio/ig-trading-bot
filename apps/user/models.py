from django.conf import settings
from django.db import models


class ExchangeAccount(models.Model):
    ACCOUNT_TYPES = (
        ('DEMO', 'Demo account'),
        ('LIVE', 'Live account')
    )

    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    exchange = models.ForeignKey(
        'trading.Exchange',
        on_delete=models.CASCADE,
        related_name='account'
    )
    alias = models.CharField(max_length=256)
    type = models.CharField(max_length=32, choices=ACCOUNT_TYPES)
    username = models.CharField(max_length=256, blank=True)
    password = models.CharField(max_length=256, blank=True)
    api_key = models.CharField(max_length=256, blank=True)
    api_secret = models.CharField(max_length=256, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.alias

    def get_type(self):
        return dict(self.ACCOUNT_TYPES)[self.type]


class Order(models.Model):
    STATUS_PENDING = 'PENDING'
    STATUS_PATTERN_FOUND = 'PATTERN_FOUND'
    STATUS_PATTERN_CONFIRMED = 'PATTERN_CONFIRMED'
    STATUS_ENTRY_SUBMITTED = 'ENTRY_SUBMITTED'
    STATUS_EXIT_SUBMITTED = 'EXIT_SUBMITTED'
    STATUS_PROCESSED = 'PROCESSED'
    STATUS_ERROR = 'ERROR'

    STATUSES = (
        (STATUS_PENDING, 'Searching for pattern...'),
        (STATUS_PATTERN_FOUND, 'Pattern found'),
        (STATUS_PATTERN_CONFIRMED, 'Pattern confirmed'),
        (STATUS_ENTRY_SUBMITTED, 'Entry submitted'),
        (STATUS_EXIT_SUBMITTED, 'Exit submitted'),
        (STATUS_PROCESSED, 'Processed'),
        (STATUS_ERROR, 'Error'),
    )

    STATUS_COLORS = {
        STATUS_PENDING: 'light-blue',
        STATUS_PATTERN_FOUND: 'blue',
        STATUS_PATTERN_CONFIRMED: 'teal',
        STATUS_PROCESSED: 'green',
        STATUS_ENTRY_SUBMITTED: 'orange',
        STATUS_EXIT_SUBMITTED: 'deep-orange lighten-2',
        STATUS_ERROR: 'red'
    }

    DIRECTION_BUY = 1
    DIRECTION_SELL = -1
    DIRECTIONS = (
        (DIRECTION_BUY, 'buy'),
        (DIRECTION_SELL, 'sell'),
    )

    CURRENCY_EURO = 'EUR'
    CURRENCY_DOLLAR = 'USD'
    CURRENCIES = (
        (CURRENCY_EURO, 'Euro'),
        (CURRENCY_DOLLAR, 'Dollar')
    )

    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    instrument = models.ForeignKey('trading.ExchangeInstrument', on_delete=models.CASCADE)
    account = models.ForeignKey('user.ExchangeAccount', on_delete=models.CASCADE)
    currency = models.CharField(choices=CURRENCIES, default=CURRENCY_EURO, max_length=3)
    size = models.DecimalField(decimal_places=2, max_digits=8)
    timeframe = models.PositiveIntegerField(choices=settings.TIMEFRAMES)
    moving_average = models.PositiveSmallIntegerField(default=14)
    direction = models.SmallIntegerField(choices=DIRECTIONS, null=True, blank=True)
    entry_price = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    exit_price = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    stoploss_price = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    deal_id = models.CharField(max_length=32, blank=True)
    deal_reference = models.CharField(max_length=32, blank=True)
    exit_deal_id = models.CharField(max_length=32, blank=True)
    exit_deal_reference = models.CharField(max_length=32, blank=True)
    status = models.CharField(max_length=32, choices=STATUSES, default=STATUS_PENDING)
    chart = models.ImageField(blank=True)
    error = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_badge_color(self):
        return self.STATUS_COLORS.get(self.status)

    def get_status(self):
        return self.status.replace('_', ' ')

    def get_timeframe(self):
        return dict(settings.TIMEFRAMES).get(self.timeframe)

    def get_position_type(self):
        position_type_map = {
            self.DIRECTION_BUY: 'Long',
            self.DIRECTION_SELL: 'Short',
        }

        return position_type_map[self.direction]
