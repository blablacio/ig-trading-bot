from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include

from apps.user.views import (
    OrderList, DeleteOrder, CreateOrder, ExchangeAccountList, CreateExchangeAccount,
    UpdateExchangeAccount, DeleteExchangeAccount,
    OrderChart, FilterTimeframes)

urlpatterns = [
    path('', include('registration.backends.default.urls')),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('order/list/', login_required(OrderList.as_view()), name='order-list'),
    path('order/<int:pk>/chart/', login_required(OrderChart.as_view()), name='order-chart'),
    path('order/create/', login_required(CreateOrder.as_view()), name='order-add'),
    path('order/<int:pk>/delete/', login_required(DeleteOrder.as_view()), name='order-delete'),
    path(
        'timeframes/<int:account>/',
        login_required(FilterTimeframes.as_view()),
        name='order-timeframes'
    ),
    path(
        'account/list/',
        login_required(ExchangeAccountList.as_view()),
        name='account-list'
    ),
    path(
        'account/create/',
        login_required(CreateExchangeAccount.as_view()),
        name='account-add'
    ),
    path(
        'account/<int:pk>/update/',
        login_required(UpdateExchangeAccount.as_view()),
        name='account-update'
    ),
    path(
        'account/<int:pk>/delete/',
        login_required(DeleteExchangeAccount.as_view()),
        name='account-delete'
    ),
]
