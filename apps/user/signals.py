import time

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django_redis import get_redis_connection

from apps.trading.tasks import pattern_scan
from apps.user.models import Order

cache = get_redis_connection('orders')


@receiver(post_save, sender=Order)
def start_order_task(instance, **kwargs):
    # Run only when an order is created
    if not kwargs.get('created'):
        return

    # Run order monitoring task only if there is no other task running
    if cache.sismember('active_orders', instance.pk):
        return

    order_data = {
        'id': instance.pk,
        'exchange': instance.account.exchange.name,
        'type': instance.account.type,
        'username': instance.account.username,
        'password': instance.account.password,
        'api_key': instance.account.api_key,
        'api_secret': instance.account.api_secret,
        'instrument': instance.instrument.identifier,
        'timeframe': instance.timeframe,
        'moving_average': instance.moving_average
    }
    result = pattern_scan(order_data)
    # Set cache
    cache.sadd('active_orders', instance.pk)
    cache.hmset(
        'order:{}'.format(instance.pk),
        {
            'task_id': result.task.id,
            'timeframe': instance.timeframe,
            'last_update': int(time.time()),
            'stage': 'scan'
        }
    )


@receiver(pre_save, sender=Order)
def validate_timeframe_choice(instance, **kwargs):
    valid_timeframes = dict(
        filter(
            lambda i: i[0] in settings.IG_SETTINGS['AVAILABLE_TIMEFRAMES'],
            settings.TIMEFRAMES
        )
    )

    if instance.timeframe not in valid_timeframes:
        raise ValidationError(
            'Timeframe "{}" is not one of the permitted values: {}'.format(
                instance.timeframe,
                ', '.join(valid_timeframes)
            )
        )
