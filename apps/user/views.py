from django.conf import settings
from django.contrib import messages
from django.http import Http404, JsonResponse
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, DeleteView, ListView, UpdateView, DetailView
from django_redis import get_redis_connection
from redis import ResponseError

from apps.trading.models import Exchange
from apps.user.models import ExchangeAccount, Order

cache = get_redis_connection('orders')


class ExchangeAccountList(ListView):
    model = ExchangeAccount

    def get_queryset(self):
        return ExchangeAccount.objects.filter(user=self.request.user).order_by('-created')


class CreateExchangeAccount(CreateView):
    model = ExchangeAccount
    fields = (
        'exchange', 'alias', 'type', 'username', 'password', 'api_key', 'api_secret'
    )
    success_message = 'Exchange account successfully created'
    success_url = reverse_lazy('account-list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        messages.success(self.request, self.success_message)

        return super().form_valid(form)


class UpdateExchangeAccount(UpdateView):
    model = ExchangeAccount
    fields = ('exchange', 'alias', 'type', 'username', 'password', 'api_key', 'api_secret')
    success_message = 'Exchange account successfully updated'
    success_url = reverse_lazy('account-list')

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = self.model.objects.filter(user=self.request.user)

        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        obj = super().post(request, *args, **kwargs)
        messages.success(self.request, self.success_message)

        return obj


class DeleteExchangeAccount(DeleteView):
    model = ExchangeAccount
    success_message = 'Exchange account successfully deleted'
    success_url = reverse_lazy('account-list')

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)

        if not obj.user == self.request.user:
            raise Http404

        return obj

    def delete(self, request, *args, **kwargs):
        obj = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)

        return obj


class OrderList(ListView):
    model = Order

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user).order_by('-created')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        order_statuses = {
            status: {
                'text': text,
                'color': Order.STATUS_COLORS[status]
            } for status, text in dict(Order.STATUSES).items()
        }

        context['order_statuses'] = order_statuses

        return context


class OrderChart(DetailView):
    model = Order

    def get(self, request, *args, **kwargs):
        order_id = kwargs.get('pk')
        since = request.GET.get('since', 0)
        data = cache.hgetall('order:{}'.format(order_id))
        last_reset = data.get('last_reset')

        # Get last reset from cache and cast to integer
        if last_reset:
            last_reset = int(last_reset)

        # If a reset flag is found in cache, return empty data set
        # with reset flag and last_reset set to the last reset time
        if data.get('reset'):
            cache.hdel('order:{}'.format(order_id), 'reset')

            return JsonResponse(
                {
                    'data': [],
                    'last_reset': last_reset,
                    'reset': True
                }
            )

        # First get crossover data; if crossover occurred, then we need
        # to get price and MA data since the beginning of the pattern
        try:
            crossover_data = cache.execute_command(
                'ts.range',
                'order:{}:chart:crossover'.format(order_id),
                0,
                -1
            )
            crossover_data = [[o[0], float(o[1])] for o in crossover_data]
        except ResponseError:
            crossover_data = []

        # Now get pattern, but show it only if crossover occurred
        if crossover_data:
            pattern_data = cache.execute_command(
                'ts.range',
                'order:{}:chart:pattern'.format(order_id),
                0,
                -1
            )
            pattern_data = [[o[0], float(o[1])] for o in pattern_data]
            # Set since to starting point of pattern
            since = pattern_data[0][0]
        else:
            pattern_data = []

        # Get price data
        try:
            price_data = cache.execute_command(
                'ts.range',
                'order:{}:chart:price'.format(order_id),
                since,
                -1
            )
            price_data = [[o[0], float(o[1])] for o in price_data]
        except ResponseError:
            # If no price data return an empty data set and last reset time
            return JsonResponse(
                {
                    'data': [],
                    'last_reset': last_reset
                }
            )

        # Allow to return empty MA as it's lagging by x periods
        try:
            ma_data = cache.execute_command(
                'ts.range',
                'order:{}:chart:ma'.format(order_id),
                since,
                -1
            )
            ma_data = [[o[0], float(o[1])] for o in ma_data]
        except ResponseError:
            # Populate MA data with nulls because HighCharts will
            # otherwise think that the series starts 14 periods later...
            ma_data = [[i[0], None] for i in price_data]

        data = {
            'data': [
                price_data,
                ma_data,
                pattern_data,
                crossover_data,
            ]
        }

        # If a pattern was found then send reset flag
        # and set last reset to beginning of pattern
        if pattern_data:
            data['last_reset'] = pattern_data[0][0]
            data['reset'] = True

        return JsonResponse(data)


class CreateOrder(CreateView):
    model = Order
    fields = (
        'account', 'instrument', 'currency', 'size', 'timeframe', 'moving_average'
    )
    success_message = 'Order successfully created'
    success_url = reverse_lazy('order-list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        messages.success(self.request, self.success_message)

        return super().form_valid(form)


class DeleteOrder(DeleteView):
    model = Order
    success_message = 'Order successfully deleted'
    success_url = reverse_lazy('order-list')

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)

        if not obj.user == self.request.user:
            raise Http404

        return obj

    def delete(self, request, *args, **kwargs):
        obj = super().delete(request, *args, **kwargs)
        messages.success(self.request, self.success_message)

        return obj


class FilterTimeframes(View):
    def get(self, request, *args, **kwargs):
        try:
            exchange = Exchange.objects.get(account=kwargs.get('account'))
        except Exchange.ObjectDoesNotExist:
            return JsonResponse({})

        exchange_settings = getattr(settings, '{}_SETTINGS'.format(exchange.name))

        if exchange_settings:
            exchange_timeframes = exchange_settings['AVAILABLE_TIMEFRAMES']
            available_timeframes = {
                timeframe: name for timeframe, name in dict(settings.TIMEFRAMES).items()
                if timeframe in exchange_timeframes
            }

            return JsonResponse(available_timeframes)

        return JsonResponse({})
