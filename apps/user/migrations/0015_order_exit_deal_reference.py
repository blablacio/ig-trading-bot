# Generated by Django 2.1.5 on 2019-02-01 15:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0014_order_exit_deal_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='exit_deal_reference',
            field=models.CharField(blank=True, max_length=32),
        ),
    ]
