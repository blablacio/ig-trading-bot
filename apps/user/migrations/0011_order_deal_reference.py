# Generated by Django 2.1.5 on 2019-01-15 01:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0010_order_chart'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='deal_reference',
            field=models.CharField(blank=True, max_length=32),
        ),
    ]
